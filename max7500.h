//
// Created by dnatov on 10/11/2021.
//

#ifndef MAX7500_H
#define MAX7500_H

#define MAX7500_FAULT1 0
#define MAX7500_FAULT2 1
#define MAX7500_FAULT4 2
#define MAX7500_FAULT6 3

#define MAX7500_TEMP_REG 0x00
#define MAX7500_CONFIG_REG 0x01
#define MAX7500_THYST_REG 0x02
#define MAX7500_TOS_REG 0x03

#include <stdbool.h>
#include <stdint.h>
#include "LL_i2c.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct MAX7500_t
{
    uint8_t Address;
    uint8_t CurrentConfiguration;
    uint16_t CurrentTos;
    uint16_t CurrentThyst;
    uint16_t LastTemperature;
    LL_I2CMaster_WriteMethod_t I2C_WriteMethodPtr;
    LL_I2CMaster_ReadMethod_t I2C_ReadMethodPtr;
};

struct MAX7500_Configuration_t
{
    bool OsActiveHigh;
    bool UseInterruptMode;
    uint8_t FaultNumber;
};

/// Initialization Function - Initializes the temp sensor and sets it off by default
/// \param instance a pointer to the instance struct
/// \param configuration a pointer to the configuration of the MAX7500
/// \param address the three bit address of the chip
void MAX7500_Configure(struct MAX7500_t* instance, struct MAX7500_Configuration_t* configuration, uint8_t address);

/// Sets the temperature parameters of the MAX7500 for behavior of OS pin
/// \param instance a pointer to the instance struct
/// \param tos Temp setting configuration for maxmimum temperature (rounded to nearest half degree)
/// \param thyst Temp setting configuration for minmimum temperature (rounded to nearest half degree)
void MAX7500_SetTosAndThyst(struct MAX7500_t* instance, float tos, float thyst);

/// Turns off/on the MAX7500 (I2C interface is still enabled when off).
/// \param instance a pointer to the instance struct
/// \param bool turn on/off
void MAX7500_TurnOnOff(struct MAX7500_t* instance, bool turnOn);

/// Gets the temperature sensed from the MAX7500
/// \param instance a pointer to the instance struct
float MAX7500_GetTemperature(struct MAX7500_t* instance);

#ifdef __cplusplus
}
#endif

#endif //MAX7500_H