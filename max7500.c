//
// Created by dnatov on 10/11/2021.
//

#include <math.h>
#include <stdlib.h>
#include "max7500.h"

#define MIN(a,b) ( a < b ) ? a : b
#define MAX(a,b) ( a > b ) ? a : b

// Converts a temperature to the registry representation. Rounded to the nearest 0.5 deg C.
static inline uint16_t _ConvertTempToRegisterValue(float tempInput)
{
    // Clamp value between -64 and 127
    tempInput = MIN(tempInput, 127);
    tempInput = MAX(tempInput, -64);

    // Get negative bit.
    bool isNegative = (tempInput < 0);

    // Multiply value by two since the LSB represents 0.5 degrees
    uint8_t temp = (uint8_t)round(abs(tempInput) * 2);

    uint16_t registerValue = 0;
    if (isNegative)
    {
        // two's complement
        temp = (~temp) + 1;
        // Set negative bit
        registerValue |= (1 << 15);
    }

    registerValue |= ((uint16_t)temp << 7);
    return registerValue;
}

void MAX7500_Configure(struct MAX7500_t* instance, struct MAX7500_Configuration_t* configuration, uint8_t address)
{
    // Set Address
    instance->Address = 0x90 | (address << 1);

    // Set up configuration register
    uint8_t configRegister = 0;
    configRegister |= (configuration->FaultNumber << 3);
    if (configuration->OsActiveHigh)
    {
        configRegister |= (1 << 2);
    }
    if (configuration->UseInterruptMode)
    {
        configRegister |= (1 << 1);
    }

    // Default chip to off
    configRegister |= (1 << 0);

    // Set up data
    uint8_t dataBuffer[2] = {0x00, 0x00};
    // Register Pointer Byte
    dataBuffer[0] = MAX7500_CONFIG_REG;
    // Configuration Byte
    dataBuffer[1] = configRegister;
    instance->CurrentConfiguration = configRegister;

    // Send
    instance->I2C_WriteMethodPtr(instance->Address, &dataBuffer[0], 2);
}

void MAX7500_SetTosAndThyst(struct MAX7500_t* instance, float tos, float thyst)
{
    uint16_t tosReg = _ConvertTempToRegisterValue(tos);
    uint16_t thystReg = _ConvertTempToRegisterValue(thyst);

    // Set TOS
    // Set up data
    uint8_t dataBuffer[3] = {0x00, 0x00, 0x00};
    // Register Pointer Byte
    dataBuffer[0] = MAX7500_TOS_REG;
    // Set Data
    dataBuffer[1] |= (tosReg >> 8);
    dataBuffer[2] |= tosReg;

    // Send
    instance->I2C_WriteMethodPtr(instance->Address, &dataBuffer[0], 3);
    instance->CurrentTos = tosReg;

    // Set THYST
    // Set up data
    uint8_t dataBuffer2[3] = {0x00, 0x00, 0x00};
    // Register Pointer Byte
    dataBuffer2[0] = MAX7500_THYST_REG;
    // Set Data
    dataBuffer2[1] |= (thystReg >> 8);
    dataBuffer2[2] |= thystReg;

    // Send
    instance->I2C_WriteMethodPtr(instance->Address, &dataBuffer2[0], 3);
    instance->CurrentThyst = thystReg;
}

void MAX7500_TurnOnOff(struct MAX7500_t* instance, bool turnOn)
{
    // Set up data
    uint8_t dataBuffer[2] = {0x00, 0x00};
    // Register Pointer Byte
    dataBuffer[0] = MAX7500_CONFIG_REG;

    // Set shutdown bit
    if (turnOn)
    {
        instance->CurrentConfiguration &= ~(1 << 0);
    }
    else
    {
        instance->CurrentConfiguration |= (1 << 0);
    }

    // Configuration Byte
    dataBuffer[1] = instance->CurrentConfiguration;

    // Send
    instance->I2C_WriteMethodPtr(instance->Address, &dataBuffer[0], 2);
}

float MAX7500_GetTemperature(struct MAX7500_t* instance)
{
    // Set register pointer address
    // Set up data
    uint8_t dataBufferPointerSet[1] = {0x00};
    // Register Pointer Byte
    dataBufferPointerSet[0] = MAX7500_TEMP_REG;

    // Send
    instance->I2C_WriteMethodPtr(instance->Address, &dataBufferPointerSet[0], 1);

    // Set recieve data
    uint8_t dataBufferRecieve[2] = {0x00, 0x00};

    // Receive
    instance->I2C_ReadMethodPtr(instance->Address, &dataBufferRecieve[0], 2);

    // Simply treat the first half of the data as a signed 8 bit integer
    int8_t temp = dataBufferRecieve[0];

    // Add half degress measurement
    bool addHalf = (dataBufferRecieve[1] == 0x80);
    float addition = addHalf ? 0.5 : 0.0;
    return ((float)temp + addition);
}